(function(factory) {

  define(function() {
    return factory(window, document);
  });

})(function(window, document) {
  var MockFunctionality = {};

  Object.defineProperties(MockFunctionality, {
    initialData: {
      value: [
        [ "02:30", "JPY", "3/3", 'Индекс расходов домохозяйств (м/м) (фев)', '2.4%', '1.1%', '-3.1%'],
        [ "02:30", "JPY", "1/3", 'Соотношение числа вакансий и соискателей (фев)', '1.3%', '1.6%', '-1.2%'],
        [ "02:30", "JPY", "2/3", 'Индекс расходов домохозяйств (м/м) (фев)', '1.3%', '0.4%', '-2.1%'],
        [ "02:30", "JPY", "1/3", 'Соотношение числа вакансий и соискателей (фев)', '1.3%', '1.6%', '-1.2%']
      ]
    },

    filteredCalendarData: {
      value: [
        [ "12:00", "JPY", "2/3", 'Индекс деловой активности в производственном секторе HSBC (сен)', '2.4%', '1.1%', '-3.1%'],
        [ "12:00", "JPY", "2/3", 'Торговый баланс (июл)', '1.3%', '1.6%', '-1.2%'],
        [ "12:00", "JPY", "2/3", 'Уровень безработицы (сен)', '1.3%', '0.4%', '-2.1%'],
        [ "12:00", "JPY", "1/3", 'Реальный объем розничной торговли (г/г) (авг)', '1.3%', '1.6%', '-1.2%']
      ]
    },

    initialTitles: {
      value: ['Время', 'Валюта', 'Важность', 'Событие', 'Факт.', 'Прогноз', 'Пред']
    },

    ajaxData: {
      value: {desc: 'Trade balance, released by Statistics New Zealand, is the difference between the value of country\'s exports and imports, over a period of year. A positive balance means that exports exceed imports, a negative ones means the opposite. Positive trade balance illustrates high competitiveness of country\'s economy.'}
    },

    initData: {
      get: function() {
        return {
          dataSet: this.initialData,
          titles: this.initialTitles
        }
      }
    },
    asyncResponse: {
      get: function() {
        return this.ajaxData;
      }
    },
    filterAsyncResponse: {
      get: function() {
        return this.filteredCalendarData;
      }
    }
  });

  return MockFunctionality;
});