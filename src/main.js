requirejs.config({
  paths: {
    'jquery': '/jquery',
    'calendar': '/calendar',
    'mock': '/mock'
  }
})

require(['jquery', 'calendar'], function($, calendar) {
  calendar.init($('#test-table'));
});
