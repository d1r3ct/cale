(function(factory) {

  define(['jquery', 'mock'], function($, mock) {
    return factory($, mock, window, document);
  });

})(function($, mockF, window, document) {
  'use strict';

  function Calendar($view, tableData, columnTitles) {
    var self = this;

    var state = $.extend(true, {}, {
      'columnTitles': columnTitles,
      'rowsData': tableData
    });

    self.oState = state;

    state.rows = [];
    // state.columns = [];

    $view.addClass(tableClassName);

    var thead = $(document.createElement('thead')).appendTo($view);
    var tbody = $(document.createElement('tbody')).appendTo($view);

    state.thead = thead[0];
    state.tbody = tbody[0];

    /*
     * Loop through the data and store the rows in the state
     */
    state.rowsData.forEach(function(rowData, rowIdx) {
      _createTr(rowData, rowIdx, state);
    });

    /*
     * Actual DOM rendering
     */
    _render(state);
  }

  /**
   * @TODO: we can allow only one opened row, by storing 'detailedView' in the state
   * Function inserts and deletes the additional "details" row after the target
   * @param {string} data Object with data from ajax request
   * @param {object} targetRow A row's object from the widget's state
   */
  Calendar.prototype.toggleDetailView = function(data, targetRow) {
    var targetRow = this.getRow(targetRow);

    // @TODO: we gotta move the html template to some global value or outside the
    // plugin, so we can easily change it
    var html = '<tr><td colspan=7>' + data.desc + '</td></tr>';

    if (targetRow._detailsShown) {
      // details are open - close it
      targetRow.detailView.remove();
    } else {
      // store details on a row property (we need to be able to remove it later)
      targetRow.detailView = $(html).insertAfter(targetRow.nTr);
    }

    // toggle the row details state
    targetRow._detailsShown = !targetRow._detailsShown;
  }

  /**
   * Returns an object tr instance
   * @param {HTMLElement} el
   */
  Calendar.prototype.getRow = function(el) {
    var rowIndex = el._AF_rowIndex;
    return this.oState.rows[rowIndex];
  }

  /**
   * Updates rows in the inner component state and calls private re-render function
   * to update the DOM
   */
  Calendar.prototype.updateRows = function(data) {
    var self = this;
    // @TODO: but we need to preserve the ids on the elements (which are set in createTr)

    // update the rows data
    self.oState.rowsData = data;

    // clear the rows state
    self.oState.rows = [];

    self.oState.rowsData.forEach(function(rowData, rowIdx) {
      _createTr(rowData, rowIdx, self.oState);
    });

    _reRenderBody(self.oState);
  }

  /*
   * Define locally scoped variables and functions
   */
  var tableClassName = 'af-economic-calendar';
  var classNames = {
    'headRow': tableClassName + '__head-row',
    'headCell': tableClassName + '__head-cell',
    'bodyRow': tableClassName + '__body-row',
    'bodyCell': tableClassName + '__body-cell',
    'detailTrig': tableClassName + '__detail-trig',
    'detailView': tableClassName + '__detail-view'
  }

  /**
   * Function creates a tr element in the state and adds its td children
   * @param {array} rowData
   * @param {int} rowIdx
   * @param {object} modelData
   */
  function _createTr(rowData, rowIdx, state) {
    var row = {};

    var tr = document.createElement('tr');
    tr.classList.add(classNames.detailTrig);

    row.nTr = tr;
    row.cells = [];
    row.index = rowIdx;

    // store the index on a DOM node
    row.nTr._AF_rowIndex = rowIdx;

    // detail view state
    row._detailsShown = false;
    row.detailView = undefined;

    rowData.forEach(function(item, idx, arr) {
      var nTd = document.createElement('td');
      nTd.classList.add(classNames.bodyCell);
      nTd._AF_cellIndex = {
        row: rowIdx,
        column: idx
      }

      nTd.innerHTML = item;

      row.cells.push(nTd);

      tr.appendChild(nTd);
    });

    // expose cells to the widget state
    state.rows.push(row);
  }

  function _reRenderBody(state) {
    // re-rendering may require addtional actions
    // that _render method does not have
    // they'll be defined here
    _render(state);
  }

  /**
   * Insert the table rows into the DOM
   * @param {object} state
   */
  function _render(state) {
    /*
     * Render table head
     */
    var tableHead = $(state.thead);
    tableHead.children().remove();

    var tableHeadRow = document.createElement('tr');
    tableHeadRow.classList.add(classNames.headRow);

    state.columnTitles.forEach(function(item, columnIdx) {
      var nTd = document.createElement('td');
      nTd.classList.add(classNames.headCell);
      nTd.innerHTML = item;

      tableHeadRow.appendChild(nTd);
    })

    tableHead.append(tableHeadRow);

    /*
     * Render table body
     */
    var tableBody = $(state.tbody);
    tableBody.children().remove();
    var rowsToRender = [];

    state.rows.forEach(function(item, idx) {
      rowsToRender.push(item.nTr);
    });
    tableBody.append($(rowsToRender));
  }

/*
  function publicApi (api) {return}

  var publicMethods = publicApi({
    publicMethod: function() {},
    _privateMethod: function(){}
  });
*/


  return {
    init: function($elem) {
      var initData = mockF.initData;

      var table = new Calendar($elem, initData.dataSet, initData.titles);

      $('#test-table').on('click', 'tr.af-economic-calendar__detail-trig', function(evt) {
        evt.preventDefault();
        var target = this;

        var asyncData = mockF.asyncResponse;
        console.log('Request to the api server, to get the data for %d element', target._AF_rowIndex);

        table.toggleDetailView(asyncData, target);
      });

      $('.js-rerender').on('click', function(evt) {
        var asyncResponse = mockF.filterAsyncResponse;
        evt.preventDefault();
        table.updateRows(asyncResponse);
      });
    }
  }

})
